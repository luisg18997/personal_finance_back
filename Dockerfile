FROM node

WORKDIR /api

COPY package*.json ./

RUN npm install

RUN npm install nodemon -g

COPY . .

EXPOSE 8080

CMD ["npm", "run", "dev"]
